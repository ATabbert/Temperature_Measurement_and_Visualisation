Still under development!

Basic idea of this project was: Have a temperature measurement in place, that 
presents the current and "historic" temperature measures in a nicely "designed" presentation.

It is based on a collection of Python programs to measure temperature 
(multiple measure points) and an according web based visualisation 
(Javascript and d3). 
The hardeware is based on Raspberry with Raspbian as OS and DS18B20 sensors.

The current project reads temperature data in a 5 minutes intervall at two measurement points:
- one in two meter height and
- one above ground

Available visualisations are:
- current temperature (2 m above ground)
- current ground temperature
- Minimum and maximum temperature of the current day
- temperature profile (min, max, average, delta t) of the last 24 hours
- temperature profile (min, max, average, delta t) of the last year
- all time temperature profile (min, max, average, delta t)
 
There are still some more things to come.

The Python and JavaScript code is partly based on examples found in the 
internet:
http://www.kompf.de/weather/pionewiremini.html
http://willy-tech.de/temperatursensor-an-raspberry-pi/#more-515

